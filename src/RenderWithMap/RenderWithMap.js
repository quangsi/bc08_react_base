import React, { Component } from "react";
import { movieArr } from "./data";
// react state , setState

export default class RenderWithMap extends Component {
  renderListMovie = () => {
    return movieArr.map((item, index) => {
      let { hinhAnh, tenPhim } = item;
      return (
        <div key={index} className="card text-left col-3">
          <img className="card-img-top" src={hinhAnh} />
          <div className="card-body">
            <p className="card-text">{tenPhim}</p>
          </div>
        </div>
      );
    });
  };
  //   missing key
  render() {
    return (
      <div className="container">
        <div className="row ">{this.renderListMovie()}</div>
      </div>
    );
  }
}
// rcc
